<?php include('assets/top.php') ?>

<h1>L’actualité de votre club</h1>

<section id="articles">
    <div class="box">
        <div class="grid">
            <figure class="effect-chico">
                <img src="images/photo_1.jpg" />
                <figcaption>
                    <h2>Le club <span>gagne !</span></h2>
                </figcaption>
            </figure>
        </div>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident reprehenderit repellendus eius accusamus.
            Provident nam commodi temporibus reiciendis quaerat numquam quas ab excepturi. Voluptatibus voluptates nam
            nisi laboriosam. In, maiores!</p>
        <button>En savoir plus</button>
    </div>
    <div class="box">
        <div class="grid">
            <figure class="effect-chico">
                <img src="images/photo_2.jpg" />
                <figcaption>
                    <h2>Match <span>perdu !</span></h2>
                </figcaption>
            </figure>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem praesentium distinctio, eligendi
            reprehenderit nostrum illum aut, nemo eaque mollitia neque sit, molestias totam? Praesentium facilis dicta,
            magni obcaecati harum alias!</p>
        <button>En savoir plus</button>
    </div>
    <div class="box">
        <div class="grid">
            <figure class="effect-chico">
                <img src="images/photo_3.jpg" />
                <figcaption>
                    <h2>Et c'est le <span>but !</span></h2>
                </figcaption>
            </figure>
        </div>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dignissimos et minima eveniet omnis accusantium,
            molestiae earum iste, totam incidunt natus dolorum excepturi animi iure sapiente at ratione. Est, magni
            quas.</p>
        <button>En savoir plus</button>
    </div>
</section>

<section id="president">
    <div class="photo"><img src="./images/photo_president.png" alt=""></div>
    <div class="citation animated bounceInRight delay-2s">
        <blockquote>
            Une équipe qui gagne, c’est une équipe qui gagne. orem ipsum dolor sit amet, consectetur adipiscing elit.
            Nullam cursus diam eget odio consectetur, a suscipit lacus dapibus. Vivamus nec nisl nunc. Duis in urna et
            metus pulvinar hendrerit vitae quis risus.
        </blockquote>

        <cite>AbdelBachir Loukili</cite>
    </div>
</section>
<h1>Nos sponsors</h1>
<section id="sponsors">
    <img src="./images/logo/auchan_logo.png" alt="">
    <img src="./images/logo/carrefour_logo.png" alt="">
    <img src="./images/logo/logo_creditdunord.png" alt="">
</section>

<?php include('assets/bottom.php') ?>