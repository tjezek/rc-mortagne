<?php include('assets/top.php') ?>

        <h1>Billeterie</h1>

        <section id="ticket">

        <div class="box_ticket">
            <p><strong>Calculateur de tarif</strong>, entrer le nombre de tickets souhaité et regarder combien ça va vous coûter !</p>
        <form action="" id="ticket" method="GET">
    <label for="baby">Nombre d'enfant(s) de moins de 2 ans :</label>
    <select name="baby">
        <?php for ($i = 0; $i <= 20; $i++) {
    echo '<option value="' . $i . '">' . $i . '</option>';
}?>
    </select>
    <br />
    <label for="children">Nombre d'enfant(s) entre 2 et 8 ans :</label>
    <select name="children">
        <?php for ($i = 0; $i <= 20; $i++) {
    echo '<option value="' . $i . '">' . $i . '</option>';
}?>
    </select>
    <br />
    <label for="teenager">Nombre d'enfant(s) de plus de 8 ans :</label>
    <select name="teenager">
        <?php for ($i = 0; $i <= 20; $i++) {
    echo '<option value="' . $i . '">' . $i . '</option>';
}?>
    </select>
    <br />
    <label for="adult">Nombre d'adulte(s) plus de 18 ans :</label>
    <select name="adult">
        <?php for ($i = 0; $i <= 20; $i++) {
    echo '<option value="' . $i . '">' . $i . '</option>';
}?>
    </select>
    <br />
    <input type="submit" name="envoyer" value="Calculer">
</form>
</div>
<div class="box_ticket">
<?php
$baby = isset($_GET['baby']) ? $_GET['baby'] : null;
$children = isset($_GET['children']) ? $_GET['children'] : null;
$teenager = isset($_GET['teenager']) ? $_GET['teenager'] : null;
$adult = isset($_GET['adult']) ? $_GET['adult'] : null;

function total_price($b, $c, $t, $a)
{

    $baby_price = 0;
    $children_price = 12.50;
    $teenager_price = 13.50;
    $adult_price = 15;

    $calcul = ($b * $baby_price) + ($c * $children_price) + ($t * $teenager_price) + ($a * $adult_price);

    if ($calcul == 0) {
        echo "";
    } else {
        echo '<br/>Il y en a pour : <strong>' . $calcul . '</strong>€';
    }
}

if ($baby == 0) {
    echo "";
} else {
    echo '- ' . $baby . ' enfants de moins de 2 ans <br/>';
}

if ($children == 0) {
    echo "";
} else {
    echo '- ' . $children . ' enfants entre 2 ans et 8 ans <br/>';
}

if ($teenager == 0) {
    echo "";
} else {
    echo '- ' . $teenager . ' enfants de plus de 8 ans <br/>';
}

if ($adult == 0) {
    echo "";
} else {
    echo '- ' . $adult . ' adultes <br/>';
}

total_price($baby, $children, $teenager, $adult);

?>
</div>
        </section>
<?php include('assets/bottom.php') ?>