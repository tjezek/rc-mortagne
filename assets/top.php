<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Accueil - RC Mortagne</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <link rel="stylesheet" href="grid.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alice" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

</head>

<body>
    <main>
        <nav>
            <a class="logo" href="">Racing Club <span>Mortagne</span></a>
            <ul id="menu">
                <li><a href="index.php">Accueil</a></li>
                <li><a href="team.php">Les équipes</a></li>
                <li><a href="event.php">Les évènements</a></li>
                <li><a href="billetterie.php">Billetterie</a></li>
                <li><a href="#">Infos pratiques</a>
                    <ul>
                        <li><a href="contact.php">Nous contacter</a></li>
                        <li><a href="plan.php">Plan d'accès</a></li>
                    </ul>
                </li>
            </ul>
        </nav>