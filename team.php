<?php include('assets/top.php') ?>

<h1>L'équipe du club</h1>

<section id="terrain">
    <p>Cliquez sur la zone du terrain pour afficher les joueurs qui correspondent</p>
    <div class="legend">
        <span class="goal">Gardiens</span>
        <span class="defense">Défenseurs</span>
        <span class="middle">Milieux</span>
        <span class="attack">Attaquants</span>
    </div>
    <img src="./images/terrain.png" usemap="#image-map">

    <map name="image-map">
        <area target="" alt="goal" title="goal" href="?goal=on" coords="48,308,129,109" shape="rect">
        <area target="" alt="defense" title="defense" href="?defense=on" coords="132,45,250,369" shape="rect">
        <area target="" alt="middle" title="middle" href="?middle=on" coords="249,45,377,370" shape="rect">
        <area target="" alt="attack" title="attack" href="?attack=on" coords="377,45,494,375" shape="rect">
        <area target="" alt="goal" title="goal" href="?goal=on" coords="496,109,580,308" shape="rect">
    </map>

    <form action="" method="GET">
        <label for="staff">Afficher le staff :</label>
        <input type="submit" name="staff" value="Afficher">
    </form>
</section>

<section id="team">
    <?php if (isset($_GET['goal']) == 'on') { ?>
    <div class="player_box">
        <p class="title">Gardien</p>
        <p><strong>Matz SELLS</strong></p>
        <p>Né le 26/02/1992</p>
        <p>à Lint (Belgique)</p>
        <span>Au club depuis juillet 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Gardien</p>
        <p><strong>Bingourou KAAMARA</strong></p>
        <p>Né le 21/10/1996</p>
        <p>à Longjumeau</p>
        <span>Au club depuis juillet 2017</span>
    </div>
    <?php } else {}  ?>

    <?php if (isset($_GET['defense']) == 'on') { ?>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Kenny LALALA</strong></p>
        <p>Né le 03/10/1991</p>
        <p>à Villepinte</p>
        <span>Au club depuis juin 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Pablo TARTINEZ</strong></p>
        <p>Né le 21/02/1989</p>
        <p>à Martigues</p>
        <span>Au club depuis juin 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Stefan MITRUVIC</strong></p>
        <p>Né le 22/05/1990</p>
        <p>à Belgrade (Serbie)</p>
        <span>Au club depuis juin 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Lamine KONA</strong></p>
        <p>Né le 01/02/1989</p>
        <p>à Paris</p>
        <span>Au club depuis août 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Lionel CAROLIN</strong></p>
        <p>Né le 12/04/1991</p>
        <p>à Montreuil</p>
        <span>Au club depuis août 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Abdallah NIDOUR</strong></p>
        <p>Né le 20/12/1993</p>
        <p>à Rufisque (Sénégal)</p>
        <span>Au club depuis juillet 2014</span>
    </div>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Duplexe TCHAMBA</strong></p>
        <p>Né le 10/07/1998</p>
        <p>à Yaoundé (Cameroun)</p>
        <span>Au club depuis mars 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Mohamed SIMAKAN</strong></p>
        <p>Né le 03/05/2000</p>
        <p>à Marseille</p>
        <span>Passé pro en mai 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Défenseur</p>
        <p><strong>Ismaël ANELBA</strong></p>
        <p>Né le 29/05/1999</p>
        <p>à Mantes-la-Jolie</p>
        <span>Au club depuis décembre 2016</span>
    </div>
    <?php } else {}  ?>

    <?php if (isset($_GET['middle']) == 'on') { ?>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Ibrahim SISSOKO</strong></p>
        <p>Né le 27/10/1997</p>
        <p>à Meaux</p>
        <span>Au club depuis juin 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Jonas MARTIN</strong></p>
        <p>Né le 09/04/1990</p>
        <p>à Besançon</p>
        <span>Au club depuis juillet 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Benjamin CORUGNET</strong></p>
        <p>Né le 06/04/1987</p>
        <p>à Thionville</p>
        <span>Au club depuis juillet 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Anthony GONÇALVES</strong></p>
        <p>Né le 06/03/1986</p>
        <p>à Chartres</p>
        <span>Au club depuis juillet 2016</span>
    </div>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Jérémy GRIM</strong></p>
        <p>Né le 27/03/1987</p>
        <p>à Ostheim</p>
        <span>Au club depuis 2013</span>
    </div>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Dimitri LENARD</strong></p>
        <p>Né le 13/02/1988</p>
        <p>à Belfort</p>
        <span>Au club depuis 2013</span>
    </div>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Moataz ZEMZEMIZ</strong></p>
        <p>Né le 07/08/1999</p>
        <p>à Tunis (Tunisie)</p>
        <span>Au club depuis janvier 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Youssouf FOFONA</strong></p>
        <p>Né le 10/01/1999</p>
        <p>à Paris</p>
        <span>Au club depuis juin 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Milieu</p>
        <p><strong>Sanjin PRCICA</strong></p>
        <p>Né le 20/11/1993</p>
        <p>à Belfort</p>
        <span>Au club depuis janvier 2019</span>
    </div>
    <?php } else {}  ?>

    <?php if (isset($_GET['attack']) == 'on') { ?>
    <div class="player_box">
        <p class="title">Attaquant</p>
        <p><strong>Nuno DA COSTA</strong></p>
        <p>Né le 10/02/1991</p>
        <p>à Praia (Cap-Vert)</p>
        <span>Au club depuis juillet 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Attaquant</p>
        <p><strong>Idriss SAADI</strong></p>
        <p>Né le 08/02/1992</p>
        <p>à Valence</p>
        <span>Au club depuis juillet 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Attaquant</p>
        <p><strong>Ludovic AJORQUE</strong></p>
        <p>Né le 25/02/1994</p>
        <p>à Saint-Joseph (La Réunion)</p>
        <span>Au club depuis juin 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Attaquant</p>
        <p><strong>Lebo MOTHIBA</strong></p>
        <p>Né le 28/01/1996</p>
        <p>à Johannesbourg (Afrique du Sud)</p>
        <span>Au club depuis août 2018</span>
    </div>
    <div class="player_box">
        <p class="title">Attaquant</p>
        <p><strong>Kevin ZOHI</strong></p>
        <p>Né le 19/12/1996</p>
        <p>à Lopou (Côte d'Ivoire)</p>
        <span>Au club depuis janvier 2017</span>
    </div>
    <div class="player_box">
        <p class="title">Attaquant</p>
        <p><strong>Samuel GRANDSIR</strong></p>
        <p>Né le 14/08/1996</p>
        <p>à Evreux</p>
        <span>Au club depuis janvier 2019</span>
    </div>
    <?php } else {}  ?>

    <?php if (isset($_GET['staff']) == 'on') { ?>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Thierry LAUREY</strong></p>
        <p>Né le 17/02/1964</p>
        <p>à Troyes</p>
        <span>Entraîneur</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Fabien LEFEVRE</strong></p>
        <p>Né le 14/11/1971</p>
        <p>à Montpellier</p>
        <span>Entraîneur-Adjoint</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Sébastien ROI</strong></p>
        <p>Né le 02/08/1978</p>
        <p>à Valence</p>
        <span>Entraîneur-Adjoint</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Jean-Yves HOURS</strong></p>
        <p>Né le 28/12/1964</p>
        <p>à Alès</p>
        <span>Entraîneur des gardiens</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Florian BAILLEUX</strong></p>
        <p>Né le 18/06/1987</p>
        <p>à Boulogne-sur-Mer</p>
        <span>Préparateur Physique</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Guillaume JAHIER</strong></p>
        <p>Né le 27/11/1988</p>
        <p>à Vannes</p>
        <span>Préparateur Physique</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Guy FEIGENBRUGEL</strong></p>
        <p>Né le 26/02/1974</p>
        <p>à Strasbourg</p>
        <span>Team Manager</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>François PIETRA</strong></p>
        <p>Né le 15/05/1960</p>
        <p>à Saint-Dizier</p>
        <span>Médecin</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Antoine ROTH</strong></p>
        <p>Né le 27/01/1986</p>
        <p>à Dambach-la-Ville</p>
        <span>Kinésithérapeuthe</span>
    </div>
    <div class="player_box">
        <p class="title">Staff</p>
        <p><strong>Franck GARNIER</strong></p>
        <p>Né le 13/09/1982</p>
        <p>à Strasbourg</p>
        <span>Ostéopathe</span>
    </div>
    <?php } else {}  ?>

</section>
<div class="background_team">
    <div class="background_team_cover"></div>
</div>
<?php include('assets/bottom.php') ?>