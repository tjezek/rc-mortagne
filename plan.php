<?php include('assets/top.php') ?>
<h1>Localiser votre club</h1>
<section id="map">
    <div class="box_map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2540.776375716059!2d3.43243131516853!3d50.445265879474626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2e89381425177%3A0x69210c634234536a!2sRue+des+Anciens+d&#39;Afrique+du+N%2C+59230+Saint-Amand-les-Eaux!5e0!3m2!1sfr!2sfr!4v1554971119589!5m2!1sfr!2sfr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="box_photo">
        <p>
        Rue des Anciens d'Afrique du N, 
        59230 Saint-Amand-les-Eaux
        </p>
    </div>
</section>
<?php include('assets/bottom.php') ?>